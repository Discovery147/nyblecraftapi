const mysql = require('mysql');
const config = require('../config');

exports.findUser = function (req, callback) {
    if (!req.query.firstName) {
        return callback({status: 403, success: false});
    } else {
        const connection = mysql.createConnection(config.dbConfig);
        connection.query('SELECT * FROM user WHERE firstName = ?', [req.query.firstName], function (error, results, fields) {
            if (error) {
                return callback({status: 403, success: false});
            } else {
                if (results.length !== 0) {
                    return callback({status: 200, success: true, data: results});
                } else {
                    return callback({status: 200, success: false, error: 'INVALID_DATA'});
                }
            }
        });
    }
};



exports.updatePDFUser = function (user_id, doc) {
    const buffers = [];
    doc.on('data', buffers.push.bind(buffers));
    doc.on('end', function () {
        const connection = mysql.createConnection(config.dbConfig);
        connection.query('UPDATE user SET pdf = ? WHERE user_id = ?', [Buffer.concat(buffers), user_id]);
    });
};
