const PDFDocument = require('pdfkit');
const UserController = require('../../controllers/user.controller');

exports.generatePDF = function (users) {
    let doc;
    users.forEach(function (user) {
        doc = new PDFDocument();
        doc.fontSize(20).text('First name: ' + user.firstName, 100, 100);
        doc.fontSize(20).text('Last name: ' + user.lastName, 100, 150);
        doc.image(user.image, 100, 190);
        doc.fontSize(5).text('i like to programming', 100, 250);
        doc.end();
        UserController.updatePDFUser(user.user_id, doc)
    });
};