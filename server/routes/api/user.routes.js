const Router = require('express');
const UserController = require('../../controllers/user.controller');
const docGenerator = require('../../service/documents/generator');
const router = new Router();

// get User by firstName
router.route('/user').get(function (req, res) {
    UserController.findUser(req, function(result) {
        res.status(result.status).json(result);
    });
});

// generate PDF
router.route('/generate').get(function (req, res) {
    UserController.findUser(req, function(result) {
        if(result.success){
            docGenerator.generatePDF(result.data);
            delete result.data;
        }
        res.status(result.status).json(result);
    });
});

module.exports = router;
